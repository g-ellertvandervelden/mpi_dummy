# MPI_dummy
# Copyright (C) 2016  Theo Steininger
#
# Author: Theo Steininger
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from setuptools import setup

exec(open('mpi_dummy/version.py').read())

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "mpi_dummy",
    version = __version__,
    author = "Theo Steininger",
    author_email = "theos@mpa-garching.mpg.de",
    description = ("A dummy module emulating MPI functionality."),
    keywords = "parallelization, MPI",
    url = "https://gitlab.mpcdf.mpg.de/ift/mpi_dummy",
    packages=['mpi_dummy'],
    zip_safe=False,
    long_description=read('README.rst'),
    license = "GPLv3",
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Topic :: Utilities",
        "License :: OSI Approved :: GNU General Public License v3 "
        "or later (GPLv3+)"
    ],
)

