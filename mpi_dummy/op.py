# -*- coding: utf-8 -*-
from builtins import object


class Op(object):
    @classmethod
    def Create(cls, function, commute=False):
        pass

MIN = Op()
MAX = Op()
SUM = Op()
PROD = Op()
LAND = Op()
LOR = Op()
BAND = Op()
BOR = Op()
